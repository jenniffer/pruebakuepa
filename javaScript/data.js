var datos = [
 {
 "id": "1",
 "nombres": "MARIA",
 "sede": "Alamos",
 "fechaIngreso":"22/06/2019"
 },
 {
 "id": "2",
 "nombres": "KARL",
 "sede": "Alamos",
 "fechaIngreso":"22/06/2019"
 },
 {
 "id": "3",
 "nombres": "BILL",
 "sede": "Restrepo",
 "fechaIngreso":"22/06/2019"
 },
 {
 "id": "4",
 "nombres": "LOLA",
 "sede": "Restrepo",
 "fechaIngreso":"10/01/2019"
 },
 {
 "id": "5",
 "nombres": "AGUSTIN",
 "sede": "Restrepo",
 "fechaIngreso":"10/01/2019"
 },
 {
 "id": "6",
 "nombres": "DANIEL",
 "sede": "Restrepo",
 "fechaIngreso":"10/01/2019"
 },
 {
 "id": "7",
 "nombres": "CAMILA",
 "sede": "Calle 72",
 "fechaIngreso":"10/01/2019"
 },
 {
 "id": "8",
 "nombres": "ANTONIO",
 "sede": "Calle 72",
 "fechaIngreso":"10/01/2019"
 },
 {
 "id": "9",
 "nombres": "RAUL",
 "sede": "Calle 72",
 "fechaIngreso":"10/01/2019"
 },
 {
 "id": "10",
 "nombres": "ANDREA",
 "sede": "Calle 72",
 "fechaIngreso":"10/01/2019"
 },
{
 "id": "11",
 "nombres": "SERGIO",
 "sede": "Calle 72",
 "fechaIngreso":"10/01/2019"
 }
];

var encabezado = '<tr>'+
'<th>ID</th>'+
'<th>Nombres</th>'+
'<th>sede</th>'+
'</tr>';

function filtrar(){
  var dataFiltrada = [];
  var filtroFechaI ="";
  var filtroFechaF = "";
  var filtroSede = document.getElementById('sedes').value;
  if (filtroSede != 'Seleccione una Sede...') {
       dataFiltrada = datos.filter(dato => dato.sede == filtroSede);  
  }else if (document.getElementById('fechaInicio') != null && document.getElementById('fechaFin') != null) {
      filtroFechaI = document.getElementById('fechaInicio').value;
      filtroFechaF = document.getElementById('fechaFin').value;
      dataFiltrada = datos.filter(dato => dato.fechaIngreso > filtroFechaI);  
  }
  
  if (dataFiltrada.length > 0) {
    for (var i = 0; i < dataFiltrada.length; i++) {
       encabezado+= '<tr>'+
       '<td>'+dataFiltrada[i].id+'</td>'+
       '<td>'+dataFiltrada[i].nombres+'</td>'+
       '<td>'+dataFiltrada[i].sede+'</td>'+
       '</tr>';
     }
   $("#tabla").append(encabezado);
   document.getElementById('sedes').value= 'Seleccione una Sede...';
   dataFiltrada.length=0;
  }else{
       $("#modalInfo").modal("show");
  }
  
}

$(function(){
  $('.input-group.date').datepicker({
      calendarWeeks: true,
      todayHighlight: true,
      autoclose: true
  });  
});


