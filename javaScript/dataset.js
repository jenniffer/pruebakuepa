function myOnLoad() {
 cargar_sedes()
}

function cargar_sedes() {
 var array = ["Restrepo", "Alamos", "Calle 72"];
 array.sort();

 addOptions("sedes", array);
}
function addOptions(domElement, array) {
	var select = document.getElementsByName(domElement)[0];

	for (value in array) {
		var option = document.createElement("option");
  		option.text = array[value];
  		select.add(option);
 	}
 }